package top.haizhi.mapdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.Poi;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class SelectActivity extends AppCompatActivity implements AMap.OnPOIClickListener {

    private MapView mapView;
    private Poi currentPOI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        mapView = findViewById(R.id.mapView);
        mapView.getMap().setOnPOIClickListener(this);

        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onPOIClick(Poi poi) {
        List<Marker> markers = mapView.getMap().getMapScreenMarkers();
        for (Marker marker : markers) {
            marker.remove();
        }

        currentPOI = poi;
        mapView.getMap().addMarker(new MarkerOptions()
                .position(poi.getCoordinate())
                .title(poi.getName())
                .snippet(poi.getCoordinate().toString())).showInfoWindow();
    }

    public void onPOISelect(View view) {
        if (currentPOI != null) {
            setResult(RESULT_OK, new Intent().putExtra("POI_DATA", currentPOI));
            finish();
            return;
        }

        Snackbar.make(view, "选择一个地点，然后继续", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
}

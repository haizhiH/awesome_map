package top.haizhi.mapdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.amap.api.maps.model.Poi;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.DriveStep;
import com.amap.api.services.route.RideRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.WalkRouteResult;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 非常简单
 * 思路是这样的，界面上放了俩按钮，用来选择起点和终点
 * 起点和终点选择完成后开始规划路线
 * 然后把结果展示在 ListView 上
 * <p>
 * 嗯，大概就是这样，注意把 AndroidManifest.xml 文件中配置的 dev_key 配置成你的开发密钥
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Point of Interest，兴趣点，一个 POI 可代表一栋大厦、一家商铺、一处景点等等。参考：https://lbs.amap.com/api/android-sdk/guide/map-data/poi/
     * <p>
     * startPOI 是选择的起点的位置
     */
    private Poi startPOI;

    /**
     * 同 startPOI，是选择的终点的位置
     */
    private Poi terminalPOI;

    /**
     * 界面中显示车辆路线的组件
     */
    private ListView pathListView;

    /**
     * 路线适配器，被设置在 pathListView 上
     */
    private SimpleAdapter pathsAdapter;

    /**
     * 行车路线数据，高德地图规划好后的数据转化而成，转换方法参见 updatePath 函数
     */
    private List<Map<String, String>> paths;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        paths = new ArrayList<>();
        pathListView = findViewById(R.id.list_main_show_path);
        pathsAdapter = new SimpleAdapter(this, paths, android.R.layout.simple_list_item_2, new String[]{"title", "info"}, new int[]{android.R.id.text1, android.R.id.text2});
        pathListView.setAdapter(pathsAdapter);
    }

    /**
     * 此方法在 activity_main 中被定义
     * 当界面的"选择起点"按钮被点击时，此方法将会被调用
     *
     * @param view "选择起点"按钮
     */
    public void selectStart(View view) {
        startActivityForResult(new Intent(this, SelectActivity.class), 0x001);
    }

    /**
     * 此方法在 activity_main 中被定义
     * 当界面的"选择终点"按钮被点击时，此方法将会被调用
     *
     * @param view "选择终点"按钮
     */
    public void selectTerminal(View view) {
        startActivityForResult(new Intent(this, SelectActivity.class), 0x002);
    }

    /**
     * 将规划好的数据显示到 ListView 上
     *
     * @param driveRouteResult 高德地图 API 搜索到的路径结果
     */
    private void updatePath(DriveRouteResult driveRouteResult) {
        paths.clear();

        List<DrivePath> drivePaths = driveRouteResult.getPaths();
        for (DrivePath drivePath : drivePaths) {
            List<DriveStep> driveSteps = drivePath.getSteps();
            for (DriveStep step : driveSteps) {
                // 关于 DriveStop 请查阅官方 API ：http://a.amap.com/lbs/static/unzip/Android_Map_Doc/index.html?Search/com/amap/api/services/route/class-use/DriveStep.html
                HashMap<String, String> itemStopMap = new HashMap<>();
                itemStopMap.put("title", step.getRoad() + "(" + step.getOrientation() + ")");
                itemStopMap.put("info", step.getInstruction());

                paths.add(itemStopMap);
            }
        }

        pathsAdapter.notifyDataSetChanged();
    }

    /**
     * 选择 POI 后更新按钮文本，显示选择的 POI 名称
     */
    private void updateButtons() {
        if (startPOI == null) {
            ((MaterialButton) findViewById(R.id.btn_main_selectStart)).setText("选择起点");
        } else {
            ((MaterialButton) findViewById(R.id.btn_main_selectStart)).setText("起点位置：" + startPOI.getName());
        }

        if (terminalPOI == null) {
            ((MaterialButton) findViewById(R.id.btn_main_selectTerminal)).setText("选择终点");
        } else {
            ((MaterialButton) findViewById(R.id.btn_main_selectTerminal)).setText("终点位置：" + terminalPOI.getName());
        }
    }

    /**
     * 开始进行路线规划
     */
    public void routeQuery() {
        RouteSearch routeSearch = new RouteSearch(this);
        RouteSearch.DriveRouteQuery query = new RouteSearch.DriveRouteQuery(new RouteSearch.FromAndTo(
                new LatLonPoint(startPOI.getCoordinate().latitude, startPOI.getCoordinate().longitude),
                new LatLonPoint(terminalPOI.getCoordinate().latitude, terminalPOI.getCoordinate().longitude)),
                0, null, null, "");
        routeSearch.setRouteSearchListener(new RouteSearch.OnRouteSearchListener() {
            @Override
            public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {

            }

            @Override
            public void onDriveRouteSearched(DriveRouteResult driveRouteResult, int i) {
                updatePath(driveRouteResult);
            }

            @Override
            public void onWalkRouteSearched(WalkRouteResult walkRouteResult, int i) {

            }

            @Override
            public void onRideRouteSearched(RideRouteResult rideRouteResult, int i) {

            }
        });
        routeSearch.calculateDriveRouteAsyn(query);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) {
            return;
        }

        Poi poi = data.getParcelableExtra("POI_DATA");
        if (requestCode == 0x001) {
            startPOI = poi;
        } else if (requestCode == 0x002) {
            terminalPOI = poi;
        }

        updateButtons();

        // 如果起点和终点都被定义，就开始计算路线
        if (startPOI != null && terminalPOI != null)
            routeQuery();
    }
}
